<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Marco Monteiro</title>
	<link rel="stylesheet" href="public/css/styles.css" type="text/css" media="screen" >
	<script type="application/javascript" src="public/js/awesomechart.js"> </script>
	<script src="http://cdn.jquerytools.org/1.2.6/full/jquery.tools.min.js"></script>
	<script>
		$(function() {
			$("ul.tabs").tabs("div.panes > div.pane");
			$("ul.tabs").tabs("div.panes > div.pane", {
				history: true,
				effect: 'fade',
				current: 'active'
			});
		});
	</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-11591891-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body>
<div class="main">
	<div class="inner-main header">
		<h1 class="logo"><span>Marco Monteiro</span></h1>
		<ul class="main-navigation">
			<li class="contact"><a id="unobtrusive" href="http://blog.marcomonteiro.net"><span>Blog</span></a></li>
		</ul>
		<ul class="main-navigation tabs">
			<li class="about">
				<a class="active" href="#about"><span>About Me</span></a>
			</li>
			<li class="experience"><a href="#experience"><span>Experience</span></a></li>
			<li class="code"><a href="#code"><span>Code</span></a></li>
		</ul>
		<div class="sub-navigation">
		<ul>
			<li class="blog"><a target="_blank" href="http://blog.marcomonteiro.net/"></a><span>Blog</span></li>
			<li class="dailypancake"><a target="_blank" href="http://dailypancake.tumblr.com"></a><span>Dailypancake</span></li>
			<li class="forrst"><a target="_blank" href="http://forrst.me/marcogmonteiro"></a><span>Forrst</span></li>
			<li class="plus"><a target="_blank" href="https://github.com/mpmont"></a><span>Github</span></li>
			<li class="bitbucket"><a target="_blank" href="https://bitbucket.org/mpmont"></a><span>Bitbucket</span></li>
			<li class="behance"><a target="_blank" href="http://www.behance.net/marcomonteiro"></a><span>Behance</span></li>
			<li class="linkedin"><a target="_blank" href="http://pt.linkedin.com/in/marcomonteir0"></a><span>Linkedin</span></li>
			<li class="zerply"><a target="_blank" href="http://zerply.com/marcomonteiro/public"></a><span>Zerply</span></li>
			<li class="tumblr"><a target="_blank" href="http://picsome.tumblr.com"></a><span>Tumblr</span></li>
			<li class="twitter"><a target="_blank" href="https://twitter.com/#!/marcogmonteiro"></a><span>Twitter</span></li>
			<li class="facebook"><a target="_blank" href="https://www.facebook.com/marcogmonteiro"></a><span>Facebook</span></li>
			<li class="foursquare"><a target="_blank" href="https://foursquare.com/marcogmonteiro"></a><span>Foursquare</span></li>
			<li class="vimeo"><a target="_blank" href="https://vimeo.com/marcogmonteiro"></a><span>Vimeo</span></li>
			<li class="skype"><a href="skype:marcogomesmonteiro?add"></a><span>Skype</span></li>
		</ul>
	</div>
	</div>
</div>
<div class="main content">
	<div class="panes inner-main body">
		<div class="pane">
			<div class="left-column">
				<h2>I am one of those hybrid-type creatives</h2>
				<p>I've been working as a web developer and designer for over 3 years. I first started as a Front-end developer, working with XHTML, CSS and JavaScript.</p>
				<p>After that I've done some work with Joomla and Wordpress, building some custom plugins, components and templates. Now I work mostly as a full PHP developer building rich applications with CodeIgniter.</p>
				<div class="graph">
					<canvas id="chartCanvas7" width="200" height="200"></canvas>
				</div>
				<div class="list-skils">
					<ul class="skils">
						<li class="html">html</li>
						<li class="css">css</li>
						<li class="js">javascript</li>
						<li class="php">php</li>
					</ul>
					<ul class="skils">
						<li class="sql">mysql</li>
						<li class="ruby">ruby</li>
						<li class="ror">ruby on rails</li>
						<li class="ci">codeigniter</li>
					</ul>
				</div>
				<div class="history">
				<h3>A little bit more about me</h3>
				<a class="prev browse left"></a>
				<img class="baby" src="./public/images/baby.png" alt="I was born"/>
				<div class="scrollable">
				   <div class="items">
				      <div class="st">
				        <span class="label">I was born</span>
				      </div>
				      <div class="nd">
				       	<span class="label">Got into school</span>
				      </div>
				      <div class="rd">
				         <span class="label">Got into Arts School</span>
				      </div>
				       <div class="th">
				         <span class="label">Accepted in Multimedia & Design</span>
				      </div>
				      <div class="wta">
				         <span class="label">Started internship at Wta</span>
				      </div>
				      <div class="awake">
				         <span class="label">Started working at Awake</span>
				      </div>
				      <div class="bloo">
				         <span class="label">Currently working at Bloo</span>
				      </div>
				   </div>
				</div>
				<a class="next browse right"></a>
				<img class="old" src="./public/images/old.png" alt="I will be old"/>
				</div><!--
				<div class="last_tweet">
					<h3 class="tweet">Last Tweet</h3>
					<p id="tweet"></p>
				</div> -->
			</div>
			<div class="right-column">
				<img class="avatar" src="./public/images/avatar.png" alt="avatar"/>
				<br/><br/><br/>
				<h3>Proof that I am legit</h3>
				<p>Yes, I know there are a ton of web designers out there, but I have proof that I have the experience and education needed! Click on the button in the footer below for my mobile CV.</p>
				<h3>Contact</h3>
				<p><a style="color: #485868;" href="mailto:marco@marcomonteiro.net">marco[a]marcomonteiro.net</a></p>
				<h3>Donate</h3>
				<p>If you are using some of my code or just appreciate the craftmanship behind it, please buy me a beer! Otherwise, I will be stuck drinking that nasty clear stuff, water.</p>
				<!--<a class="get_me_drunk" href="">buy me a beer</a>-->
				<form style="width:133px;height:37px;background:none;" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="hosted_button_id" value="RVG547UE2F92J">
					<input type="image" src="http://dl.dropbox.com/u/404972/marcomonteiro/donation-button.png" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					<img class="paypal" alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="0" height="0">
				</form>

			</div>
		</div>
		<div class="pane">
			<div class="left-column">
				<h2>Experience</h2>
				<div class="experience-company bloo">
					<img class="company-logo" src="./public/images/bloo.png" alt="bloo"/>
					<h4 class="from_to">Web Designer / Developer <span>@ Bloo</span><br/>
					<span class="dates">July 2010 - Present</span></h4>
				</div>
				<div class="experience-company awake">
					<img class="company-logo" src="./public/images/awake.png" alt="bloo"/>
					<h4 class="from_to">Web Designer <span>@ Awake</span><br/>
					<span class="dates">Oct 2009 - May 2010</span></h4>
				</div>
				<div class="experience-company wta">
					<img class="company-logo" src="./public/images/wta.png" alt="bloo"/>
					<h4 class="from_to">Web Designer / Developer <span>@ Wta</span><br/>
					<span class="dates">Set 2008 - Set 2009</span></h4>
				</div>
				<div class="left-column education">
					<h2>Education</h2>
					<div class="experience-company">
						<h4 class="from_to">Bachelor of Design and Multimedia<br/>
						<span class="dates">2005 - 2009</span></h4>
					</div>
				</div>
			</div>
			<div class="right-column">
				<p class="experience_description">Currently I'm still working there as a Webdeveloper. In the first few months I had some small projects with Wordpress and Joomla but after that I made a custom content management system and from that point on the company only works with that. <br/><br/>
				All my work now is done with my custom CMS and with the help Codeigniter a PHP / MVC framework.</p>
				<p class="experience_description">
				I was still doing a lot of work with Joomla. I had evolved much since wta, Comunicação, was already making templates, components and modules from scratch. I even customized some back-ends.
				<br/><br/>
				I started coding from scratch and build some websites without using any CMS or framework.
				</p>
				<p class="experience_description">
				First started with simple newsletter management and soon moved on, making simple web applications.
				<br/><br/>
				After a few weeks I was managing all their websites built with Joomla 1.0.
				<br/><br/>
				When my internship ended I staid there working as a Junior Joomla Developer. Making some websites from scratch with this content management system (Joomla).</p>
			</div>
		</div>
		<div class="pane">
			<div class="left-column">
				<div class="code-block first-one">
					<span class="height">
						<img style="max-width: 100px" src="http://dl.dropbox.com/u/404972/marcomonteiro/thumbnail-icon.png"/>
					</span>
					<div class="code-description">
						<h3><a href="https://github.com/mpmont/thumb_helper" title="thumb helper" target="_balnk">Thumb-Helper</a></h3>
						<p>
							I’ve created a small thumbnail helper for codeigniter and I’d like to get some feedback from you guys.
						</p>
					</div>
				</div>
				<br/><br/><br/>
				<div class="code-block">
					<span class="height">
						<img style="max-width: 100px" src="http://dl.dropbox.com/u/404972/marcomonteiro/youtube_vimeo_thumb.png"/>
					</span>
					<div class="code-description">
						<h3><a href="https://github.com/mpmont/video_helper" title="thumb helper" target="_balnk">Video-Helper</a></h3>
						<p>
							I’ve written a small helper since latterly almost all of my projects have to work with youtube or even with vimeo. This is a helper to work with Codeigniter obviously.
						</p>
					</div>
				</div>
				<br/><br/><br/>
				<div class="code-block">
					<span class="height">
						<img style="max-width: 100px" src="http://dl.dropbox.com/u/404972/marcomonteiro/sublimeicon.png"/>
					</span>
					<div class="code-description">
						<h3><a href="https://github.com/mpmont/ci-snippets" title="thumb helper" target="_balnk">Codeigniter Snippets</a></h3>
						<p>
							Using Sublime text 2 and Codeigniter? What are you waiting then?
						</p>
					</div>
				</div>
				<br/><br/><br/>
				<div class="code-block">
					<span class="height">
						<img style="margin: 0 20px 0 10px; border: 2px solid #fff" src="http://placekitten.com/80/80"/>
					</span>
					<div class="code-description">
						<h3><a href="https://github.com/mpmont/adopt-kitten" title="Adopt Kitten" target="_balnk">Adopt Kitten</a></h3>
						<p>
							Need a placeholder? Like Kittens? Use Codeigniter? You're welcome.
						</p>
					</div>
				</div>
			</div>
			<div class="right_column">
				<!--<div class="first-one last-feed">
					<p style="margin-bottom: 10px;"><small>Lattest blog post</small></p>
					<h3>
					<?= anchor($feed_entry['link'], $feed_entry['title']) ?>
					</h3>
					<p><?= $feed_entry['description'] ?></p>
				</div>-->
			</div>
		</div>
		<div class="pane">
			<div class="contact-sep">
				<h2>Send me a note.</h2>
				<p class="form-description">
				Drop me a line about anything. Open-source projects, Codeigniter, freelancing, or even just saying hello.
				</p>
				<?= form_open('welcome/sendmail', 'class="send_message"') ?>
					<div class="half">
						<label>Name</label><br/>
						<input type="text" name="name" pattern="[a-zA-Z ]{5,}" maxlength="100" required="required"/>
					</div>
					<div class="half">
						<label>E-mail Address</label><br/>
						<input type="email" name="email" required="required" />
					</div>
					<div class="full">
						<label>Message</label><br/>
						<textarea name="message"></textarea>
					</div>
					<div class="half">
						<div class="bot">
						<p class="bot">4 + 1 =
							<input name="bot" class="text" maxlength="1" style="width: 40px" required="required" />
						</p>
						</div>
					</div>
					<div class="contact half">
						<input type="submit" value="Send"/>
					</div>
				<?= form_close() ?>
			</div>
			<div class="contact-right">
				<h2>Or...</h2>
				<p class="contacts_email_phone"><span>e-mail:</span> <a href="mailto:marco@marcomonteiro.net">marco[a]marcomonteiro.net</a><br/>
				<span>phone:</span> +351 917 057 804</p>
				<a class="vcard" href="http://h2vx.com/vcf/http://zerply.com/marcomonteiro">Download my V-card</a>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="inner-main">
			<div class="download-cv">
				<a href="http://dl.dropbox.com/u/404972/marcomonteiro/cv_marcomonteiro_en.pdf">Download my CV</a>
			</div>
			<p class="copyright">
				Copyright &#169; 2012 <a href="mailto:marco@marcomonteiro.net">Marco Monteiro</a> &#8226; All Rights Reserved &#8226; Design by <a target="_blank" href="http://phostercreative.com/">Brian Pesinger</a>
			</p>

		</div>
	</div>
</div>
<script type="application/javascript">
    var chart7 = new AwesomeChart('chartCanvas7');
	    chart7.chartType = "ring";
	    chart7.title = "";
	    chart7.data = [10,10,6,10,7,4,5,10];
	    //chart7.labels = ['IE','Firefox','Chrome','Safari','Opera','Other'];
	    chart7.colors = ['#a7c7e4', '#7ab8f0', '#636363', '#304554', '#879db3', '#58687a', '#a9a9a9', '#404040'];
	    chart7.randomColors = false;
	    chart7.draw();
	    $(function() {
			// initialize scrollable
			$(".scrollable").scrollable();
		});
	});
</script>
</body>
</html>